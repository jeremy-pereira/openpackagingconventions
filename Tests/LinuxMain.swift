import XCTest

import OpenPackagingConventionsTests

var tests = [XCTestCaseEntry]()
tests += OpenPackagingConventionsTests.allTests()
XCTMain(tests)
