//
//  DataSamples.swift
//  
//
//  Created by Jeremy Pereira on 19/10/2019.
//

import Foundation


/// Manages the data samples for the tests
struct DataSamples
{
	/// The bundle in which the files are stored.
	let resourceDir: URL

	init()
	{
		let fileURL = URL(fileURLWithPath: #file)
		resourceDir = fileURL.deletingLastPathComponent().appendingPathComponent("resources")
	}


	/// Open a resource as an `InputStream`
	/// - Parameter resourceName: The resource name
	/// - Parameter ext: The resource extension
	/// - Returns: An input stream for reading the resource
	func openAsStream(resourceName: String, extension ext: String) throws -> InputStream
	{
		let binaryFileUrl = try url(resourceName: resourceName, extension: ext)
		guard let ret = InputStream(url: binaryFileUrl)
			else { throw Error.cannotOpen(binaryFileUrl) }
		return ret
	}


	/// Get a file URL for a resource
	/// - Parameter resourceName: The name of the resource
	/// - Parameter ext: The extension of the resource
	/// - Returns: a file url for the resource.
	func url(resourceName: String, extension ext: String) throws -> URL
	{
		let binaryFileUrl = resourceDir
								.appendingPathComponent(resourceName)
								.appendingPathExtension(ext)
		guard FileManager.default.fileExists(atPath: binaryFileUrl.path)
		else { throw Error.cannotLocateResource(binaryFileUrl) }
		return binaryFileUrl
	}

	static let defaults = DataSamples()
}

extension  DataSamples
{
	/// Errors trhat might be thrown while locating or oprenin g a resource
	enum Error: Swift.Error
	{
		case cannotLocateResource(URL)
		case cannotOpen(URL)
	}
}
