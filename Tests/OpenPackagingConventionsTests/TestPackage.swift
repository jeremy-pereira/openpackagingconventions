//
//  TestPackage.swift
//  
//
//  Created by Jeremy Pereira on 19/10/2019.
//

import XCTest
import Toolbox
@testable import OpenPackagingConventions

private let log = Logger.getLogger("OpenPackagingConventions.OpenPackagingConventionsTests")

typealias PartName = OPCPackage.Part.Name
final class TestPackage: XCTestCase
{

	private func fetchDataURL(resourceName: String, extension: String) -> URL
	{
		do
		{
			return try DataSamples.defaults.url(resourceName: resourceName, extension: `extension`)
		}
		catch
		{
			fatalError("Failed to get \(resourceName).\(`extension`) from the test bundle: \(error)")
		}
	}

	func testGetPartsByName()
	{
		let sampleDocxInfo: [PartName : PartInfo] =
		[
			try! PartName(string: "/_rels/.rels")                  : PartInfo(contentType: "application/vnd.openxmlformats-package.relationships+xml"),
			try! PartName(string: "/word/_rels/document.xml.rels") : PartInfo(contentType: "application/vnd.openxmlformats-package.relationships+xml"),
			try! PartName(string: "/word/document.xml")            : PartInfo(contentType: "application/vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml"),
			try! PartName(string: "/word/media/image1.gif")        : PartInfo(contentType: "image/gif"),
			try! PartName(string: "/word/theme/theme1.xml")        : PartInfo(contentType: "application/vnd.openxmlformats-officedocument.theme+xml"),
			try! PartName(string: "/word/settings.xml")            : PartInfo(contentType: "application/vnd.openxmlformats-officedocument.wordprocessingml.settings+xml"),
			try! PartName(string: "/word/webSettings.xml")         : PartInfo(contentType: "application/vnd.openxmlformats-officedocument.wordprocessingml.webSettings+xml"),
			try! PartName(string: "/docProps/core.xml")            : PartInfo(contentType: "application/vnd.openxmlformats-package.core-properties+xml"),
			try! PartName(string: "/word/styles.xml")              : PartInfo(contentType: "application/vnd.openxmlformats-officedocument.wordprocessingml.styles+xml"),
			try! PartName(string: "/word/fontTable.xml")           : PartInfo(contentType: "application/vnd.openxmlformats-officedocument.wordprocessingml.fontTable+xml"),
			try! PartName(string: "/docProps/app.xml")             : PartInfo(contentType: "application/vnd.openxmlformats-officedocument.extended-properties+xml"),
		]

		log.pushLevel(.debug)
		defer { log.popLevel() }

		let file = fetchDataURL(resourceName: "sample", extension: "docx")
		do
		{
			let package = try OPCPackage.open(uri: file, access: .read)
			XCTAssert(package.parts.count == sampleDocxInfo.count, "Wrong package count \(package.parts.count)")
			for part in package.parts.values
			{
				guard let partInfo = sampleDocxInfo[part.name] else { XCTFail("Package has extra part \(part.name)") ; continue }
				XCTAssert(partInfo.contentType == part.contentType, "\(part.name) has content type '\(part.contentType)', expected '\(partInfo.contentType)'")
				// TODO: A check to make sure that the expected parts are all there.

			}
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testPackageRelationships()
	{
		let sampleDocXInfo: [String : RelInfo] =
		[
			"rId1" : RelInfo(type: "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument", relativePath: "word/document.xml", mode: .internal),
			"rId2" : RelInfo(type: "http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties", relativePath: "docProps/core.xml", mode: .internal),
			"rId3" : RelInfo(type: "http://schemas.openxmlformats.org/officeDocument/2006/relationships/extended-properties", relativePath: "docProps/app.xml", mode: .internal),
		]
		Logger.pushLevel(.debug, forName: "OpenPackagingConventions")
		defer { Logger.popLevel(forName: "OpenPackagingConventions") }
		
		let file = fetchDataURL(resourceName: "sample", extension: "docx")
		do
		{
			let package = try OPCPackage.open(uri: file, access: .read)
			XCTAssert(package.relationships.count == 3, "Wrong relationship count \(package.relationships.count)")
			for relationship in package.relationships
			{
				guard let relInfo = sampleDocXInfo[relationship.id]
					else { XCTFail("Relationship \(relationship.id) should not exist") ; continue }
				XCTAssert(relationship.mode == relInfo.mode, "\(relationship.id): Invalid mode \(relationship.mode)")
				XCTAssert(relationship.type == relInfo.type, "\(relationship.id): Invalid type \(relationship.type)")
				XCTAssert(relationship.target == relInfo.relativePath, "\(relationship.id): Invalid target \(relationship.target)")
			}
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testPartRelationships()
	{
		let sampleDocXInfo: [String : RelInfo] =
		[
			"rId1" : RelInfo(type: "http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles", relativePath: "styles.xml", mode: .internal),
			"rId2" : RelInfo(type: "http://schemas.openxmlformats.org/officeDocument/2006/relationships/settings", relativePath: "settings.xml", mode: .internal),
			"rId3" : RelInfo(type: "http://schemas.openxmlformats.org/officeDocument/2006/relationships/webSettings", relativePath: "webSettings.xml", mode: .internal),
			"rId4" : RelInfo(type: "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image", relativePath: "media/image1.gif", mode: .internal),
			"rId5" : RelInfo(type: "http://schemas.openxmlformats.org/officeDocument/2006/relationships/fontTable", relativePath: "fontTable.xml", mode: .internal),
			"rId6" : RelInfo(type: "http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme", relativePath: "theme/theme1.xml", mode: .internal),
		]
		Logger.pushLevel(.debug, forName: "OpenPackagingConventions")
		defer { Logger.popLevel(forName: "OpenPackagingConventions") }
		
		let file = fetchDataURL(resourceName: "sample", extension: "docx")
		let package = try! OPCPackage.open(uri: file, access: .read)
		do
		{
			let documentParts = package.targetParts(ofType: "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument")
			guard let documentPart = documentParts.first
				else { XCTFail("Unable to find a document part") ; return }
			XCTAssert(documentPart.relationships.count == 6, "Wrong relationship count \(documentPart.relationships.count)")
			for relationship in documentPart.relationships
			{
				guard let relInfo = sampleDocXInfo[relationship.id]
					else { XCTFail("Relationship \(relationship.id) should not exist") ; continue }
				XCTAssert(relationship.mode == relInfo.mode, "\(relationship.id): Invalid mode \(relationship.mode)")
				XCTAssert(relationship.type == relInfo.type, "\(relationship.id): Invalid type \(relationship.type)")
				XCTAssert(relationship.target == relInfo.relativePath, "\(relationship.id): Invalid target \(relationship.target)")
				XCTAssert(documentPart.targetParts(ofType: relationship.type).count == 1, "Cannot find part for relation of type '\(relationship.type)'")
			}
		}

	}

	func testCoreProperties()
	{
		let file = fetchDataURL(resourceName: "sample", extension: "docx")

	}

//    static var allTests = [
//        ("testExample", testExample)
//    ]
}

extension  TestPackage
{
	struct PartInfo
	{
		let contentType: String
	}

	struct RelInfo
	{
		let type: String
		let relativePath: String
		let mode: OPCPackage.Relationship.TargetMode
	}
}
