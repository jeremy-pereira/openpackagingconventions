# OpenPackagingConventions

This is a Swift implementation of Microsoft's OpenPackagingConventions. An overview can be found at:

[https://wiki.openoffice.org/wiki/OOXML/Open_Packaging_Conventions_(OPC)](https://wiki.openoffice.org/wiki/OOXML/Open_Packaging_Conventions_(OPC))

This is intended to be the basis for reading and writing Microsoft Excel documents.

