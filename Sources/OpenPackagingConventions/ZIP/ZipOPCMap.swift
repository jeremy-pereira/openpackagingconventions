//
//  File.swift
//  
//
//  Created by Jeremy Pereira on 21/10/2019.
//
import ZIPFoundation
import Foundation

/// Provides a mapping between an OPCPackage and a raw zip archive
///
/// Not everything in a ZIP archive should be visdible at the OPC level. For
/// example, the content types in a zip entry called `[ContentTypes].xml`
/// should not be visible.
struct ZipOPCMap: Archiving
{
	var accessMode: OPCPackage.Access
	{
		switch archive.accessMode
		{
		case .create:
			return .create
		case .read:
			return .read
		case .update:
			return .readWrite
		}
	}


	private var archive: ZIPFoundation.Archive

	private var rawEntries: [EntryContext]

	init(archive: ZIPFoundation.Archive) throws
	{
		self.archive = archive
		rawEntries = try archive.map{ try EntryContext(archive: archive, entry: $0) }
	}

	static let contentTypesName = try! OPCPackage.Part.Name(string: "/[Content_Types].xml")
	/// The entries that an OPCPackage should be aware of
	var entries: [ArchivingEntry]
	{
		return rawEntries.filter
		{
			$0.name != ZipOPCMap.contentTypesName
		}
	}

	func makeContentTypesMap() throws -> OPCPackage.ContentTypesMap
	{
		// Get the root element and make sure it conforms to spec.
		guard let contentTypeXmlFile = rawEntries.first(where: { $0.name == ZipOPCMap.contentTypesName })
		else { throw OPCPackage.Error.noContentTypesAvailable("ZIP should contain [Content_types].xml") }
		let xmlDocument = try XMLDocument(inStream: try contentTypeXmlFile.makeInStream(), options: [])
		guard let typesElement = xmlDocument.rootElement()
			else { throw OPCPackage.Error.noContentTypesAvailable("[Content_types].xml has no 'Types' element") }
		guard typesElement.localName == "Types" && typesElement.uri == ZipOPCMap.contentTypesNamespace
			else {throw OPCPackage.Error.noContentTypesAvailable("[Content_types].xml 'Types' element has incorrect name or namespace") }

		// TODO: Ensure extensions and part names are unique
		// First get the defaults
		let defaultElements = typesElement.elements(forLocalName: "Default", uri: ZipOPCMap.contentTypesNamespace)
		var defaultsMap: [String : String] = [:]
		for element in defaultElements
		{
			guard let extensionString = element.attribute(forName: "Extension")?.stringValue
				else { throw OPCPackage.Error.noContentTypesAvailable("[Content_types].xml 'Default' element has no Extension") }
			guard let contentTypeString = element.attribute(forName: "ContentType")?.stringValue
				else { throw OPCPackage.Error.noContentTypesAvailable("[Content_types].xml 'Default' element has no content type") }
			defaultsMap[extensionString] = contentTypeString
		}

		// Now get the overrides
		let overrideElements = typesElement.elements(forLocalName: "Override", uri: ZipOPCMap.contentTypesNamespace)
		var overridesMap: [OPCPackage.Part.Name : String] = [:]
		for element in overrideElements
		{
			guard let partNameString = element.attribute(forName: "PartName")?.stringValue
				else { throw OPCPackage.Error.noContentTypesAvailable("[Content_types].xml 'Default' element has no PartName") }
			guard let contentTypeString = element.attribute(forName: "ContentType")?.stringValue
				else { throw OPCPackage.Error.noContentTypesAvailable("[Content_types].xml 'Default' element has no content type") }
			let partName = try OPCPackage.Part.Name(string: partNameString)
			overridesMap[partName] = contentTypeString
		}
		return OPCPackage.ContentTypesMap(defaults: defaultsMap, overrides: overridesMap)
	}

	static let contentTypesNamespace = "http://schemas.openxmlformats.org/package/2006/content-types"
}

extension ZipOPCMap
{
	struct EntryContext: ArchivingEntry
	{

		let archive: Archive
		let entry: Entry
		let name: OPCPackage.Part.Name

		init(archive: Archive, entry: Entry) throws
		{
			self.archive = archive
			self.entry = entry
			self.name = entry.path.hasPrefix("/") ? try OPCPackage.Part.Name(string: entry.path)
												  : try OPCPackage.Part.Name(string: "/" + entry.path)

		}

		func makeInStream() throws -> InStream
		{
			let ret = EntryInStream()
			// TODO: Check the CRC
			try _ = archive.extract(entry, consumer: { ret.add(data: $0) })
			return ret
		}
	}

	/// An `inStream` that works by reading the entire archive entry into
	/// memory and then dispensing slices off it.
	fileprivate class EntryInStream: InStream
	{
		var data: Data = Data()
		var readOffset: Int = 0

		init()
		{
			// Nothing to be done
		}

		fileprivate func add(data: Data)
		{
			self.data.append(data)
		}

		func available() -> Int
		{
			return data.count - readOffset
		}

		func mark(readLimit: Int)
		{
			// Do nothing
		}

		var markSupported: Bool { return false }

		func readAllBytes() -> Data
		{
			defer { readOffset = data.count }
			return data.subdata(in: readOffset ..< data.count)
		}

		func close()
		{
			readOffset = data.count
		}

		func read() -> UInt8?
		{
			guard readOffset < data.count else { return nil }
			defer { readOffset += 1 }
			return data[readOffset]
		}

		func read(bytes: inout Data, offset: Int, length: Int) -> Int
		{
			guard offset >= 0 && length >= 0 && offset + length < bytes.count
				else { fatalError("Index out of bounds for input buffer") }

			guard readOffset < data.count else { return -1 }
			let actualByteCount = Swift.min(data.count - readOffset, length)
			bytes[offset ..< offset + actualByteCount] = data[readOffset ..< actualByteCount]
			readOffset += actualByteCount
			return actualByteCount
		}
	}

}

extension OPCPackage.Access
{
	var zipAccess: Archive.AccessMode
	{
		switch self
		{
		case .read:
			return .read
		case .readWrite:
			return .update
		case .create:
			return .create
		}
	}
}

