//
//  OPCPackage+Relationship.swift
//  
//
//  Created by Jeremy Pereira on 05/11/2019.
//
import Foundation

/// Protocol adopted by types that can act as the source of a relationship
public protocol RelationshipSource: class
{
	/// The package that the source is in
	var package: OPCPackage { get }

	/// What is the part containing the relationships
	///
	/// It's valid for a package part not to have a corresponding relationship
	/// part, in which case `nil` is returned and the `relationships` list will
	/// be empty.
	var relationshipPart: OPCPackage.Part? { get }

	/// The actual relationships
	///
	/// It's valid for a package part not to have a corresponding relationship
	/// part, in which case an empty list is returned and the `relationshipPart`
	/// will be nil.
	var relationships: [OPCPackage.Relationship] { get }


	/// Find all the relation parts with the given relation type
	///
	/// A default implementation of this function is provided that filters the
	/// relationships. Implementations may override with a more performant
	/// solution
	///
	/// - Parameter type: Type of the relation
	/// - Returns: An array of all the parts that are targets of the given
	///            relation type.
	func targetParts(ofType type: String) -> [OPCPackage.Part]


	/// Find the part targetted by the given relationship
	///
	/// If the relationship is external or points to a non existent part, we
	/// return nil.
	///
	/// A default implementation is provided.
	/// - Parameter relationship: The relationship
	/// - Returns: The part if it exists. If it doesn't exist, there is a
	///            dangling relsationship - or it is external.
	func targetPart(forRelationship relationship: OPCPackage.Relationship) -> OPCPackage.Part?


	/// True if this sourcer has any relationships.
	///
	/// A default implementation is provided that returns `relationships.count > 0`
	var hasRelationships: Bool { get }

	/// Resolves the relative name
	///
	/// It may be impossible to resolve the relative name e.g. `../foo` from
	/// the package root makes no sense, if so, we return `nil`
	/// - Parameter name: The relative part name
	/// - Returns: The part name if we can.
	func partName(forRelativeName name: String) -> OPCPackage.Part.Name?
}

extension RelationshipSource
{
	public func targetParts(ofType type: String) -> [OPCPackage.Part]
	{
		let relativeNames = relationships.filter { $0.type == type && $0.mode == .internal }
										 .map { $0.target }
		let partNames = relativeNames.compactMap { partName(forRelativeName: $0) }
		return partNames.compactMap { package.parts[$0] }
	}

	public var hasRelationships: Bool { relationships.count > 0 }

	public func targetPart(forRelationship relationship: OPCPackage.Relationship) -> OPCPackage.Part?
	{
		guard relationship.mode == .internal else { return nil }

		guard let targetPartName = partName(forRelativeName: relationship.target)
			else { return nil }
		return package.parts[targetPartName]
	}
}


public extension OPCPackage
{
	/// Models a relationship between a source and a target.
	struct Relationship
	{
		private(set) weak var source: RelationshipSource!

		/// Namespace for relationship description XML
		public static let namespace = "http://schemas.openxmlformats.org/package/2006/relationships"
		/// Relationship type for core properties.
		public static let corePropertiesType = "http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties"

		/// Relationship type for core office document
		public static let coreDocumentType = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument"
		/// Relationship type for strict core documents
		public static let strictCoreDocumentType = "http://purl.oclc.org/ooxml/officeDocument/relationships/officeDocument"
		/// Relationship type for style parts
		public static let stylePartType = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles"

		/// The extension for a relationship part
		static let ext = "rels"
		/// The name of the directory containing relationship parts
		static let dirSegment = try! Part.Name.Segment("_rels")

		/// Is this relationship internal or external
		public let mode: TargetMode
		/// Ther unique within the package id
		public let id: String
		/// The target of the relationship
		public let target:String
		/// The type of the relationship
		public let type: String


		/// Denotes whether a relationship is internal to the package or
		/// refers to an external target e.g. a URL
		public enum TargetMode: String
		{
			case `internal` = "Internal"
			case external = "External"
		}

		fileprivate init(source: RelationshipSource,
						 mode: TargetMode,
						 id: String,
						 target: String,
						 type: String) throws
		{
			self.source = source
			self.mode = mode
			self.id = id
			self.target = target
			self.type = type
			guard mode != .internal || source.partName(forRelativeName: target) != nil
				else { throw OPCPackage.Error.invalidRelationship("Internal target cannot be resolved") }
		}

		/// Parse the relationships for the given relationship source
		///
		/// It's possible that a relationship source has no relationships, in
		/// which case, it may not have a relationship part. We return an empty
		/// array in that scenario.
		/// - Parameter source: The relationship source
		/// - Returns: An array of all the relatinships
		/// - Throws: If the content of the part cannot be read or if it fails
		///           validation.
		public static func parseRelationships(source: RelationshipSource) throws -> [Relationship]
		{
			guard let relationshipPart = source.relationshipPart else { return [] }

			let inStream = try relationshipPart.makeInStream()
			defer { inStream.closeQuietly(failAction: { _ in }) }

			let xml = try XMLDocument(inStream: inStream)
			guard let root = xml.rootElement()
				else { throw OPCPackage.Error.invalidRelationship("Relationship xml has no root node") }
			let relationshipElements = root.elements(forLocalName: "Relationship",
													 uri: OPCPackage.Relationship.namespace)
			var ret: [Relationship] = []
			for element in relationshipElements
			{
				guard let targetMode = TargetMode(rawValue: element.attribute(forName: "TargetMode")?.stringValue ?? "Internal")
					else { throw Error.invalidRelationship("Invalid target mode specified") }
				guard let id = element.attribute(forName: "Id")?.stringValue
					else { throw Error.invalidRelationship("No id specified") }
				guard let target = element.attribute(forName: "Target")?.stringValue
				else { throw Error.invalidRelationship("No target specified") }
				guard let type = element.attribute(forName: "Type")?.stringValue
				else { throw Error.invalidRelationship("No type specified") }
				ret.append(try Relationship(source: source,
										    mode: targetMode,
										    id: id,
										    target: target,
										    type: type))
			}
			return ret
		}

		/// The target of the relationship as a URI string.
		///
		/// If it is external, we return the target, which is assumed to be a
		/// URI. If it is internal, we make it absolute relative to the package
		/// and return it.
		public var targetUriString: String
		{
			if mode == .external
			{
				return target
			}
			else
			{
				guard let resolvedTarget = source.partName(forRelativeName: target)
					else { fatalError("Incorrectly assumed that the target must be legal") }
				return resolvedTarget.description
			}
		}

	}

	/// Prints all of the relationships for all parts to an output stream
	/// - Parameter stream: The stream to print to
	func printAllRelationships<T: TextOutputStream>(to stream: inout T)
	{
		relationships.forEach
		{
			stream.write("'/' -> '\($0.target)'\n")
		}
		parts.values.forEach
		{
			part in
			part.relationships.forEach
			{
				stream.write("'\(part.name)' -> \($0.target)\n")
			}
		}
	}
}
