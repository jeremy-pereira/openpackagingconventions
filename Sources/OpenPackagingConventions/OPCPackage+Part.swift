//
//  File.swift
//  
//
//  Created by Jeremy Pereira on 20/10/2019.
//
import Toolbox

private let log = Logger.getLogger("OpenPackagingConventions.OPCPackage+Part")

extension OPCPackage
{
	/// A package part
	open class Part
	{
		/// Defines the part's relationships
		public internal(set) var relationships: [OPCPackage.Relationship] = []
		/// The name of this part
		public var name: Name { entry.name }

		private let entry: ArchivingEntry
		private weak var ownerPackage: OPCPackage!

		init(package: OPCPackage, archivingEntry: ArchivingEntry) throws
		{
			self.ownerPackage = package
			self.entry = archivingEntry
		}

		open var contentType: String
		{
			guard let contentType =  ownerPackage.contentType(forName: name)
				else { fatalError("Should not be calling this without a content type") }
			return contentType
		}

		/// Make an new input stream for the content of the part.
		///
		/// Each time this is called, a new stream is made. It is the
		/// responsibility of the caller to close the stream.
		/// - Returns: An `Instream` containing the content of the part
		/// - Throws: If, for any reason, the input stream cannot be created.
		open func makeInStream() throws -> InStream
		{
			return try entry.makeInStream()
		}
	}
}

extension OPCPackage.Part
{

	/// Models a part name.
	///
	/// Part names have the following syntax:
	///
	/// ```
	/// partName = 1*("/" segment)
	///  segment = 1*(pchar)
	/// ```
	/// where `pchar` is a restricted set of characters. While we are just
	/// reading OPC packages, we will assume that the characxters in the part
	/// name are always legal.
	///
	/// - TODO: Test for `pchar` characters
	/// - TODO: Test for percent encoded `/`
	/// - TODO: Test for illegal chars percent encoded.
	///
	/// - Throws: if the string violates the requirements for a part name
	public struct Name
	{
		/// A segment of a part name
		struct Segment: Hashable, CustomStringConvertible
		{
			let value: String

			init<S: StringProtocol>(_ string: S) throws
			{
				guard !string.hasSuffix(".") && !string.isEmpty
					else { throw OPCPackage.Error.invalidName("Name segment cannot be empty or end with a '.': \(string)") }

				value = String(string)
			}

			var description: String { value }

			var ext: String?
			{
				if let dotPosition = value.lastIndex(of: ".")
				{
					let extStart = value.index(after: dotPosition)
					return String(value[extStart ..< value.endIndex])
				}
				else
				{
					return nil
				}
			}

		}

		private let segments: [Segment]

		/// Initialise from a string
		///
		/// Some checks are performed to make sure the name is legal. But, some
		/// necessary checks are not done here because they depend on other
		/// context. e.g. a part cannot be a sub part of an existing part.
		/// - Parameter string: The string to initialise from
		/// - Throws: if we detect an invalid part name
		public init<S: StringProtocol>(string: S) throws
		{
			let splitSegments = string.split(separator: "/", omittingEmptySubsequences: false)
			guard splitSegments.count > 0 && splitSegments[0].isEmpty
			else
			{
				throw OPCPackage.Error.invalidName("A part name must always start with `/`: \(string)")
			}
			let segments = splitSegments.dropFirst()
			try self.init(segments: segments)
		}


		/// Initialse a part name from a list of segments
		/// Some checks are performed to make sure the name is legal. But, some
		/// necessary checks are not done here because they depend on other
		/// context. e.g. a part cannot be a sub part of an existing part.
		///  - Parameter segments: A list of segments for the part name
		///  - Throws: if we detect an invalid part name
		public init<Seq: Sequence, Str: StringProtocol>(segments: Seq) throws
			where Seq.Element == Str
		{
			let convertedSegments = try segments.map { try Segment($0) }
			try self.init(segments: convertedSegments)
		}


		/// Initialise from a string of segments
		/// - Parameter segments: The segments to initialise from
		init<Seq: Sequence>(segments: Seq) throws
		where Seq.Element == Segment
		{
			self.segments = Array(segments)
			guard self.segments.count > 0
				else { throw OPCPackage.Error.invalidName("A part name must have at least one segment: \(segments)") }
		}


		/// The extension of this part name
		public var ext: String?
		{
			self.segments.last?.ext
		}

		/// The last segment of the part name
		///
		/// Part names always have at least one segment, so this cannot be `nil`
		public var lastSegment: String { return segments.last!.value }


		/// Returns a new part name but with the last segment removed.
		///
		/// - Returns: A new part name with the last segment removed. If `self`
		///            has one part, `nil` is returned because a part name
		///            cannot be empty.
		public func removingLastSegment() -> Name?
		{
			guard segments.count > 1 else { return nil }

			// We have guaranteed there were at least two segments in `self` so
			// the constructor will not throw.
			return try! Name(segments: segments.dropLast())
		}

		fileprivate func appending(_ segment: Segment) -> Name
		{
			return try! Name(segments: self.segments + [segment])
		}


		/// Find a part name relative to `self` based on the given en string path
		///
		/// Note that the "relative" part is relative to the parent part of `self`
		/// For example, if `self` is `/foo/bar` and the relative string is
		/// `baz/biz`, the new path will be `/foo/baz/biz`.
		///
		/// The relative string may contain instances of `..` to go up the
		/// path name.
		/// - Parameter string: Relative path
		/// - Returns: The part name relative to `self`
		/// - Throws: if the new part name would be illegal. e.g. `..` from `/`
		///           would take us outside the package.
		public func nameRelative<S: StringProtocol>(to string: S) throws -> Name
		{
			guard !string.hasPrefix("/")
			else
			{
				// This is actually an absolute path
				return try Name(string: string)
			}
			// sequences of "/" are considered to be equivalent to a single
			// "/" i.e. foo//bar == foo/bar
			let relativeSegmentStrings = string.split(separator: "/", omittingEmptySubsequences: true)
			var newSegments = self.segments.dropLast()
			for segmentString in relativeSegmentStrings
			{
				if segmentString == ".."
				{
					guard newSegments.count > 0
						else { throw OPCPackage.Error.invalidName("Invalid relative path '\(string)' for part name '\(self)'")}
					newSegments.removeLast()
				}
				else
				{
					newSegments.append(try Segment(segmentString))
				}
			}
			return try Name(segments: newSegments)
		}

		public var isRelationshipPartName: Bool
		{
			// First make sure the extension is correct
			guard let ext = self.ext, ext == OPCPackage.Relationship.ext
				else { return false }
			// Now the last segment apart from the final one must be `_rels`
			return segments.dropLast().last ==  OPCPackage.Relationship.dirSegment
		}
	}
}

extension OPCPackage.Part: Hashable
{
	public static func == (lhs: OPCPackage.Part, rhs: OPCPackage.Part) -> Bool
	{
		return ObjectIdentifier(lhs) == ObjectIdentifier(rhs)
	}

	public func hash(into hasher: inout Hasher)
	{
		hasher.combine(ObjectIdentifier(self))
	}
}

extension OPCPackage.Part: RelationshipSource
{
	public var package: OPCPackage
	{
		return ownerPackage
	}

	/// Get the relationship part for this part
	///
	/// The relationship part for `/path/to/part.ext` is
	/// `/path/to/_rels/part.ext.rels`
	public var relationshipPart: OPCPackage.Part?
	{
		// Nothing we do here should throw. Creating a file name from a valid
		// segment and appending a valid extension should not throw. _rels is
		// a valid segment. Inserting it into a name as a segment should not
		// throw.
		let newLastSegment = try! Name.Segment(name.lastSegment + "." + OPCPackage.Relationship.ext)

		let relationshipPartName: Name
		if let dirName = name.removingLastSegment()
		{
			relationshipPartName = dirName.appending(OPCPackage.Relationship.dirSegment)
				                          .appending(newLastSegment)
		}
		else
		{
			relationshipPartName = try! Name(segments: [newLastSegment])
		}
		return package.parts[relationshipPartName]
	}

	public func partName(forRelativeName name: String) -> OPCPackage.Part.Name?
	{
		do
		{
			return try self.name.nameRelative(to: name)
		}
		catch
		{
			log.warn("Failed to get part name for '\(name)' relative to '\(self.name)")
			return nil
		}
	}
}

extension OPCPackage.Part.Name: CustomStringConvertible
{
	public var description: String
	{
		let slashes = Array<String>(repeating: "/", count: segments.count)
		return zip(slashes, segments).reduce("") { $0 + $1.0 + $1.1.value }
	}
}

extension OPCPackage.Part.Name: Hashable
{

}
