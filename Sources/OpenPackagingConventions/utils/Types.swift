//
//  Types.swift
//  OfficeKit
//
//  Created by Jeremy Pereira on 15/09/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

public extension StringProtocol
{
	/// Find the first instance of a reg ex in a string
	/// - Parameter regEx: Regular expression to look for
	/// - Parameter range: The range in which to look or nil if the whole
	///                    sequence (defaults to nil.
	func regExRange(of regEx: String, range: Range<String.Index>? = nil) -> Range<String.Index>?
	{
		return self.range(of: regEx, options: .regularExpression, range: range, locale: nil)
	}


	/// Find the range of the first quoted string
	///
	/// The range is from the opening double quote up to and including the
	/// closing double quote. If no unescaped double quote is found in the
	/// search  range, or an openiong quote is found but not a closing quote,
	/// `nil` is returned.
	///
	/// Escape handling is quite simplistic. If you see a `\` then the next
	/// character is assumed to be escaped and if it is a double quote is
	/// ignored for the purposes of this search.
	/// - Parameter range: Range of `self` to search in or `nil` for the whole
	///                    string. Defaults to `nil`
	/// - Returns: The range of the quoted string.
	func rangeOfFirstQuotedString(range: Range<String.Index>? = nil) -> Range<String.Index>?
	{
		var index = range?.lowerBound ?? self.startIndex
		let upperBound = range?.upperBound ?? self.endIndex
		var stringStart: String.Index?
		var stringEnd: String.Index?
		var isEscaped = false
		while index < upperBound && stringEnd == nil
		{
			let char = self[index]
			switch (isEscaped, char)
			{
			case (true, _):
				isEscaped = false
			case (false, "\""):
				if stringStart == nil
				{
					stringStart = index
				}
				else
				{
					stringEnd = index
				}
			case (false, "\\"):
				isEscaped = true
			case (false, _):
				break
			}
			index = self.index(after: index)
		}
		guard let start = stringStart, let end = stringEnd else { return nil }
		return start ..< self.index(after: end)
	}
}

extension BinaryInteger
{
	@discardableResult
	public static prefix func ++(n: inout Self) -> Self
	{
		n += 1
		return n
	}

	@discardableResult
	public static prefix func --(n: inout Self) -> Self
	{
		n -= 1
		return n
	}

	@discardableResult
	public static postfix func ++(n: inout Self) -> Self
	{
		defer { n += 1 }
		return n
	}

	@discardableResult
	public static postfix func --(n: inout Self) -> Self
	{
		defer { n -= 1 }
		return n
	}
}
