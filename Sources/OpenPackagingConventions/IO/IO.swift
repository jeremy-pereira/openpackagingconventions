//
//  IO.swift
//  
//
//  Created by Jeremy Pereira on 19/10/2019.
//
import Foundation

/// Any type that needs to be closed when you finish using it
public protocol Closeable
{
	/// Clost the instance of the type
	/// Throws: Implementations may throw if something goes wrong
	func close() throws
}

/// Interface thart defines an input stream.
///
/// This is designed to look like a Java `InputStream`
public protocol InStream: Closeable
{
	/// An estimate of the amount of bytes that can be read without blocking
	/// - Returns: The count of available bytes. The actual number of bytes
	///            might be more than reported.
	/// - Throws: If there's an error.
	func available() throws -> Int

	/// Marks the current location in the input stream
    ///
	/// Marks the input stream at the current point so that, if `reset()` is
	/// called we can return to this point. If `mark` is unsupported, this may
	/// fail silently.
	/// - Parameter readLimit: Once the stream has read past the `readLimit` it
	///                        is allowed to foget the mark.
	func mark(readLimit: Int)

	/// True if mark is supported for this stream
	var markSupported: Bool { get }

	/// Read's the next byte
	///
	/// - Returns: The next byte or `nil` if we are at the end of the file.
	func read() -> UInt8?

	/// Read some bytes into a `Data` object.
	///
	/// The maximum number of bytes read is determined by the size of the byte
	/// buffer.
	///
	/// By default this function is a wrapper for
	/// ```
	/// return try read(bytes: &bytes, offset: 0, length: bytes.count)
	/// ```
	/// - Parameter bytes: A byte buffer to read into.
	/// - Returns: The number of bytes actually read or `-1` if the
	///            end of the stream has been read.
	/// - Throws: If some sort of error occurs while reading,
	func read(bytes: inout Data) throws -> Int

	/// Read some bytes into the buffer at a given offset for a given maximum
	/// length.
	///
	/// - Parameter bytes: The buffer to read data into
	/// - Parameter offset: The offset in the buffer to start putting the bytes at
	/// - Parameter length: The maximum number of bytes to read.
	/// - Returns: -1 if the end of the stream has been reasched or the number
	///            of bytes actually read.
	/// - Throws: if the offset is negative, the length is negative or
	///           `offset + length > bytes.count`, also if an IO error occurs.
	func read(bytes: inout Data, offset: Int, length: Int) throws -> Int

	/// Read all the remaining bytes on the stream.
	///
	/// - Returns: A data object containing all the remaining bytes
	/// - Throws: If there is an IO error.
	func readAllBytes() throws -> Data
}

extension InStream
{
	public func read(bytes: inout Data) throws -> Int
	{
		return try read(bytes: &bytes, offset: 0, length: bytes.count)
	}

	/// Close the input stream, supressing any errors
	///
	/// Since closing is allowed to throw, it can be a bit annoying, if you
	/// don't really care - for example, if you are closing in a `defer`.
	/// - Parameter failAction: Something to do if an error is thrown while
	///                         closing. If you use a block that throws,
	///                         `closeQuietly` will rethrow the error.
	public func closeQuietly(failAction: ((Error) throws -> ())) rethrows
	{
		do
		{
			try self.close()
		}
		catch
		{
			try failAction(error)
		}
	}
}


/// Interface that defines an output stream.
///
/// Models a Java `OutputStream`
public protocol OutStream: Closeable
{
	// TODO: Define the output stream
}

public extension XMLDocument
{
	/// Initialise an XMLDocument from a stream that, hopefully holds XML
	/// - Parameter inStream: The input stream to ini
	/// - Parameter options: XML reading options
	convenience init(inStream: InStream, options: XMLNode.Options = []) throws
	{
		let data = try inStream.readAllBytes()
		try self.init(data: data, options: options)
	}
}
