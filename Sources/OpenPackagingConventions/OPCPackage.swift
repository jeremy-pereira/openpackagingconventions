//
//  OPCPackage.swift
//  
//
//  Created by Jeremy Pereira on 19/10/2019.
//
import Foundation
import ZIPFoundation
import Toolbox

private let log = Logger.getLogger("OpenPackagingConventions.OPCPackage")

/// Represents an OPC package
open class OPCPackage
{
	private var backingStore: Archiving

	/// The parts in this package
	final private(set) var parts: [Part.Name : Part] = [:]

	/// A map between part names and content types
	private var contentTypes: OPCPackage.ContentTypesMap

	init(backingStore: Archiving) throws
	{
		self.backingStore = backingStore
		contentTypes = try backingStore.makeContentTypesMap()
		let parts = try backingStore.entries
			.map{ try Part(package: self, archivingEntry: $0) }
			.filter{ contentTypes[$0.name] != nil }
		for part in parts
		{
			self.parts[part.name] = part
		}
		// Now get the relationships for everything. At some point, we may
		// consider deferring this but it would lead to many more try
		// expressions.
		relationships = try Relationship.parseRelationships(source: self)
		for part in parts
		{
			if !part.name.isRelationshipPartName
			{
				part.relationships = try Relationship.parseRelationships(source: part)
			}
		}
	}

	public var accessMode: Access { return backingStore.accessMode }
	/// Open a package.
	///
	/// For the moment, we support only zip archives.
	///
	/// - TODO: Add support for non read only access
	///
	/// - Parameter uri: File url of package to open
	/// - Parameter access: What access should we have to the package
	open class func open(uri: URL, access: Access) throws -> OPCPackage
	{
		guard access == .read else { throw Error.unsupportedAccessMode(access) }

		guard let backingStore = ZIPFoundation.Archive(url: uri, accessMode: access.zipAccess)
		else
		{
			throw access == .create ? Error.fileAlreadyExists(uri) : Error.fileDoesNotExist(uri)
		}
		return try OPCPackage(backingStore: ZipOPCMap(archive: backingStore))
	}

	open class func open(stream: InStream) throws -> OPCPackage { notImplemented() }


	/// The content type for the part with the given neme
	///
	/// Will return `nil` if there is no content type for the given name, which
	/// may be illegal depending on who calls it.
	/// - Parameter name: The name of the pasrt for which to search a content type.
	/// - Returns: the content type for the given part name.
	func contentType(forName name: Part.Name) -> String?
	{
		return contentTypes[name]
	}

	/// Defines the package relationships
	public fileprivate(set) var relationships: [OPCPackage.Relationship] = []

	public func partsByName(pattern: String) -> [OPCPackage.Part]
	{
		return parts.compactMap
		{
			let name = $0.key
			let part = $0.value
			return name.description.range(of: pattern, options: .regularExpression) == nil ? nil : part
		}
	}
}

extension OPCPackage: RelationshipSource
{
	public var package: OPCPackage { self }


	/// Returns the part name for the relative name
	///
	/// Since this is the package, a relative name is relative to the root. We
	/// simply convert the name into a `Part.Name`
	/// - Parameter name: The relative name
	public func partName(forRelativeName name: String) -> OPCPackage.Part.Name?
	{
		do
		{
			return try Part.Name(string: name.hasPrefix("/") ? name : ("/" + name))
		}
		catch
		{
			log.error("Relative name is not valid for a part name: \(error)")
			return nil
		}
	}

	// Creates a part name of `/_rels/.rels`
	private static let relationshipPartName
		= try! Part.Name(segments: [Relationship.dirSegment,
									try! Part.Name.Segment("." + Relationship.ext)])

	public var relationshipPart: OPCPackage.Part?
	{
		log.debug("OPCPackage.relationshipPartName = \(OPCPackage.relationshipPartName)")
		return self.parts[OPCPackage.relationshipPartName]
	}
}

public extension OPCPackage
{

	/// How should the archive be opened
	enum Access
	{
		/// Read only
		case read
		/// Read and update
		case readWrite
		/// Create a new package
		case create
	}
}

public extension OPCPackage
{
	/// Packaging errors
	enum Error: Swift.Error
	{
		case unsupportedAccessMode(Access)
		case fileDoesNotExist(URL)
		case fileAlreadyExists(URL)
		case invalidName(String)
		case noContentTypesAvailable(String)
		case invalidRelationship(String)
	}
}

public extension OPCPackage
{
	struct Namespaces
	{
		private init(){}

		public static let markupCompatibility = "http://schemas.openxmlformats.org/markup-compatibility/2006"
	}
}

public extension OPCPackage
{
	/// Map of content types for the package
	struct ContentTypesMap
	{
		private var defaults: [String : String]
		private var overrides: [Part.Name : String]

		public init(defaults: [String : String], overrides: [Part.Name : String])
		{
			self.defaults = defaults
			self.overrides = overrides
		}
		subscript(name: Part.Name) -> String?
		{
			if let ret = overrides[name]
			{
				return ret
			}
			if let ext = name.ext, let ret = defaults[ext]
			{
				return ret
			}
			return nil
		}
	}
}

/// Defines a protocol for opening packages of a certain type and operatios on it
///
/// It also lets us find archiving entries for parts in the archive.
public protocol Archiving
{

	/// Create a map of content types from the archive
	///
	/// How the map is created is implementation dependent. For example, a Zip
	/// archive has an XML file in it called `[Content_types].xml
	/// - Returns: A mapping from part names to content types
	/// - Throws: If the Content type map cannot be created for whatever reason.
	func makeContentTypesMap() throws -> OPCPackage.ContentTypesMap


	/// The entries in the archive
	///
	/// These correspond to package parts at the OPC level of abstraction
	var entries: [ArchivingEntry] { get }


	/// What mode is this `Archiving` opened in
	var accessMode: OPCPackage.Access { get }
}


/// Represents one entry in an archive
public protocol ArchivingEntry
{

	/// The name of the entry in the archive.
	///
	/// This shouldf be the full path name within the archive. The archiving
	/// entry must perform any conversion between its internal names and the
	/// pasrt name conventions.
	var name: OPCPackage.Part.Name { get }


	/// Make a new instance of an input stream for the archive entry
	///
	/// The stream is a brand new stream that can be used to read the entire
	/// content of the archiving entry. The caller is responsible for closing
	/// the stream.
	/// - Throws: If the stream can't be made
	/// - Returns: The new `InStream`
	func makeInStream() throws -> InStream
}
